using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;


public class HistogramAngleSpawnerSystem : SystemBase
{
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }


    protected override void OnUpdate()
    {
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref RoundHistogramData spawner) =>
            {
                if (spawner.Created)
                    return;
                for (float angle = spawner.AngleResolution / 2; angle < 90f; angle += spawner.AngleResolution)
                {
                    var instance = commandBuffer.Instantiate(entityInQueryIndex, spawner.Prefab);
                    var position = spawner.Zero;
                    commandBuffer.SetComponent(entityInQueryIndex, instance, new Translation {Value = position});
                    commandBuffer.SetComponent(entityInQueryIndex, instance,
                        new RoundHistogramComponentData()
                        {
                            DeltaAngleDeg = spawner.AngleResolution, AngleDeg = angle, Length = 1f, StartPos = position
                        });
                    commandBuffer.SetComponent(entityInQueryIndex, instance,
                        new Rotation() {Value = quaternion.Euler(0, 0, math.radians(angle) * 2 - (math.PI / 2f))});

                }

                spawner.Created = true;

            }).ScheduleParallel();
        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

    private static quaternion ToQuaternion(float yaw, float pitch, float roll) // yaw (Z), pitch (Y), roll (X)
    {
        // Abbreviations for the various angular functions
        var cy = math.cos(yaw * 0.5f);
        var sy = math.sin(yaw * 0.5f);
        var cp = math.cos(pitch * 0.5f);
        var sp = math.sin(pitch * 0.5f);
        var cr = math.cos(roll * 0.5f);
        var sr = math.sin(roll * 0.5f);

        var w = cr * cp * cy + sr * sp * sy;
        var x = sr * cp * cy - cr * sp * sy;
        var y = cr * sp * cy + sr * cp * sy;
        var z = cr * cp * sy - sr * sp * cy;

        return new quaternion(x, y, z, w);
    }

}
