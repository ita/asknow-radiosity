using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[GenerateAuthoringComponent]
public struct RoundHistogramComponentData : IComponentData
{
    public int Number;
    public float3 StartPos;
    public float AngleDeg;
    public float DeltaAngleDeg;
    public float Length;
    public float Width;
}