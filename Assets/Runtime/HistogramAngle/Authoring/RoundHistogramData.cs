using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct RoundHistogramData : IComponentData
{
    public float AngleResolution;
    public float3 Zero;
    public bool Created;
    public Entity Prefab;
}