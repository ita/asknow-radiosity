using UnityEngine;

public static class MeshCreator
{
    public static Mesh CreateTriangle(float width, float height)
    {
        var halfWidth = width / 2f;

        var vertices = new[]
        {
            new Vector3(-halfWidth, +height),
            new Vector3(+halfWidth, +height),
            new Vector3(+0, -0)
        };
        var uv = new[]
        {
            new Vector2(0, 1),
            new Vector2(1, 1),
            new Vector2(1, 0)
        };

        var triangles = new[] {0, 1, 2};

        var mesh = new Mesh {vertices = vertices, uv = uv, triangles = triangles};

        return mesh;
    }
    
    
    public static Mesh TriangleMeshCenter(float width=1, float height=1)
    {
        Mesh mesh = new Mesh();

        //Verticies
        Vector3[] verticies = new Vector3[3]
        {
            new Vector3(-width/2,0,0), 
            new Vector3(width/2, 0, 0),
            new Vector3(0, height, 0)
        };

        //Triangles
        int[] tri = new int[3];

        tri[0] = 0;
        tri[1] = 2;
        tri[2] = 1;

        //normals
        Vector3[] normals = new Vector3[3];

        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;

        //UVs
        Vector2[] uv = new Vector2[3];

        uv[0] = new Vector2(0, 0);
        uv[0] = new Vector2(1, 0);
        uv[0] = new Vector2(0, 1);

        //initialise
        mesh.vertices = verticies;
        mesh.triangles = tri;
        mesh.normals = normals;
        mesh.uv = uv;
        return mesh;

    }
}
