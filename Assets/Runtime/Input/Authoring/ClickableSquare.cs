using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct ClickableSquare : IComponentData
{
    public float3 Scale;
}