using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct InputPointMouse : IComponentData
{
    public float3 Position;
    public bool NewRay;
    public int NumberOfRays;
}