using CodeMonkey.Utils;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


[UpdateInGroup(typeof(InputGuiSystemGroup))]
public class ClickedTagAddSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem _entityCommandBufferSystem;

    protected override void OnCreate()
    {
        _entityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var commandBuffer = _entityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            Entities.WithAll<OnHoverOverTag>().ForEach((Entity entity, int entityInQueryIndex) =>
            {
                commandBuffer.AddComponent(entityInQueryIndex, entity, new OnClickedTag());
                commandBuffer.AddComponent(entityInQueryIndex, entity, new OnClickedPressTag());
            }).ScheduleParallel();
            _entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        }


        if (Input.GetMouseButtonUp(0))
        {
            var commandBuffer = _entityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

            var startPos = new float3(UtilsClass.GetMouseWorldPosition());

            if (startPos.x < -2.13f || startPos.y < 0f)
            {
                var anythingClicked = false;
                Entities.WithAll<OnHoverOverTag>().WithNone<RoomData>().ForEach((Entity entity) =>
                {
                    anythingClicked = true;
                }).Run();
                if(!anythingClicked)
                {
                    Entities.ForEach((ref InputPointMouse inputPoint) =>
                    {
                        inputPoint.Position = startPos;
                        inputPoint.NewRay = true;
                    }).Run();
                }
            }

            Entities.WithAll<OnHoverOverTag>().ForEach((Entity entity, int entityInQueryIndex) =>
            {
                commandBuffer.RemoveComponent<OnClickedPressTag>(entityInQueryIndex, entity);
            }).ScheduleParallel();
            _entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        }

    }
}