﻿using Unity.Entities;

public struct RayPropertiesRadiosityReceiver : IComponentData
{
    public float Time;
    public float Energy;
}