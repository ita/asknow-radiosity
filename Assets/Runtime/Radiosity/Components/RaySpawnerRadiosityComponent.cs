using Unity.Entities;

[GenerateAuthoringComponent]
public struct RaySpawnerRadiosityComponent : IComponentData { 
    public Entity Prefab;
}