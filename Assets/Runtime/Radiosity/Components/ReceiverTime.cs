﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct ReceiverTime : IComponentData
{
    public bool Start;
    public float CurrentTime;
}
