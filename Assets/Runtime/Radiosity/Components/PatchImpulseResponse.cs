﻿using Unity.Entities;

public struct PatchImpulseResponse : IBufferElementData
{
    public float Energy;
    public float Distance;
}