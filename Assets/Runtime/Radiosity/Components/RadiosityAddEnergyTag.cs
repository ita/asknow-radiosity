using Unity.Entities;
using Unity.Mathematics;

public struct RadiosityAddEnergyTag : IComponentData
{
    public float3 Normal;
}