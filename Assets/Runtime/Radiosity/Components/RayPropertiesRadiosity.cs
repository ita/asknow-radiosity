﻿using Unity.Entities;

public struct RayPropertiesRadiosity : IComponentData
{
    public Entity SourcePatch;
    public float Energy;
    public int Index;
}