using Unity.Entities;
using Unity.Mathematics;

public struct MoveRayRadiosity : IComponentData
{
    public float3 Direction;
    public float Speed;
    public float Distance;
    public Entity Source;
}