﻿using Unity.Entities;

public struct PatchImpulseResponseNew : IBufferElementData
{
    public float Energy;
    public float Distance;
}