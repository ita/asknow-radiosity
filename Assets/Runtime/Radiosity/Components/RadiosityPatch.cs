using Unity.Entities;
using Unity.Mathematics;

public struct RadiosityPatch : IComponentData
{
    public float3 Normal;
    public float3 Size;
    public float Energy;
    public float UnShotEnergy;
    public float Absorption;
}