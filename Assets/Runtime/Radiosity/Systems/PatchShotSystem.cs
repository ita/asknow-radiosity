using System;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateBefore(typeof(InitialShotSystem))]
public class PatchShotSystem : SystemBase
{
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }


    protected override void OnUpdate()
    {
        var prefab = Entity.Null;
        Entities
            .ForEach((in RaySpawnerRadiosityComponent spawnerFromEntity, in Translation translation) =>
            {
                prefab = spawnerFromEntity.Prefab;
            }).Run();

        if (prefab == Entity.Null)
        {
            m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
            return;
        }

        var numParticle = 1;
        Entities.ForEach((in StartRandomRays startRandomRays) =>
            {
                numParticle = startRandomRays.Number;

            }).Run();
        // var areRaysMoving = false;
        // Entities.WithAll<MoveRayRadiosity>().ForEach(() =>
        // {
        //     areRaysMoving = true;
        //
        // }).Run();

        // if (areRaysMoving) return;
        
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        Entities.WithAll<HighestUnshotEnergyTag>()
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref RadiosityPatch patch, in Translation translation) =>
            {
                var deltaAngle = 2f * math.PI / 2f / numParticle;
                for (var i = 0; i < numParticle; i++)
                {

                    var angle = deltaAngle * (i + 0.5f);
                    var instance = commandBuffer.Instantiate(entityInQueryIndex, prefab);
                    if (Math.Abs(patch.Normal.y - 1) < 1e-5)
                        angle += 0;
                    else if (Math.Abs(patch.Normal.y + 1) < 1e-5)
                        angle += math.PI;
                    else if (Math.Abs(patch.Normal.x - 1) < 1e-5)
                        angle -= math.PI / 2f;
                    else if (Math.Abs(patch.Normal.x + 1) < 1e-5)
                        angle += math.PI / 2f;

                    var moveDirection = new float3(math.cos(angle), math.sin(angle), 0);
                    // moveDirection  = quaternion.LookRotation(patch.Normal, math.);
                    var pos = translation.Value + patch.Normal * 0.2f;
                    commandBuffer.SetComponent(entityInQueryIndex, instance,
                        new Translation {Value = pos});
                    // new Translation {Value = translation.Value});
                    commandBuffer.SetComponent(entityInQueryIndex, instance,
                        new Rotation() {Value = quaternion.RotateZ(angle + (math.PI / 2f))});
                    commandBuffer.AddComponent<TowardsPatchTag>(entityInQueryIndex, instance);
                    commandBuffer.AddComponent(entityInQueryIndex, instance,
                        new MoveRayRadiosity()
                        {
                            Direction = math.normalize(moveDirection), 
                            Speed = 1f,
                            Source = entity
                        });  
                    commandBuffer.AddComponent(entityInQueryIndex, instance,
                        new RayPropertiesRadiosity()
                        {
                            SourcePatch = entity,
                            Energy = patch.UnShotEnergy / numParticle * (1f - patch.Absorption),
                        });
                }
                // reset Energy
                patch.Energy = patch.UnShotEnergy;
                patch.UnShotEnergy = 0f;

                commandBuffer.RemoveComponent<HighestUnshotEnergyTag>(entityInQueryIndex, entity);
            }).ScheduleParallel();
        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}