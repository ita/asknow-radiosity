using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(MoveRayRadiositySystem))]
public class RenderPatchSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var renderShotEnergy = false;
        var useLogScale = false;
        var factor = 1f;
        
        Entities.ForEach((in RenderPatch renderPatch) =>
        {
            renderShotEnergy = renderPatch.RenderShotEnergy;
            factor = renderPatch.Factor;
            useLogScale = renderPatch.UseLogScale;
        }).Run();
        
        Entities.WithAll<OnClickedTag>().ForEach((ref RenderPatch renderPatch) =>
        {
            renderPatch.RenderShotEnergy = !renderPatch.RenderShotEnergy;
        }).Run();
        
        Entities.ForEach((ref Scale scale, in RadiosityPatch patch ) =>
        {
            var energy = renderShotEnergy ? patch.UnShotEnergy : patch.Energy;
            if (useLogScale)
                scale.Value = energy <= 0f ? 0f : (10f * math.log10(energy / 10f) + 100f) / 100f;
            else
                scale.Value = math.sqrt(energy / factor);
        }).ScheduleParallel();
    }
}