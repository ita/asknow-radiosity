﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateAfter(typeof(MoveRayRadiositySystem))]
public class AddRayToImpulseResponsePatchSystem : SystemBase
{
    
    private EndSimulationEntityCommandBufferSystem endSimulationEcbSystem;

     protected override void OnCreate()
     {
         base.OnCreate();
         // Find the ECB system once and store it for later usage
         endSimulationEcbSystem = World
             .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
     }

    protected override void OnUpdate()
    {
        var patchesQuery = GetEntityQuery(
            ComponentType.ReadOnly<RadiosityAddEnergyTag>(),
            ComponentType.ReadOnly<MoveRayRadiosity>(),
            ComponentType.ReadOnly<RayPropertiesRadiosity>(),
            ComponentType.ReadOnly<Translation>());
        var numEntities = patchesQuery.CalculateEntityCount();

        if (numEntities < 1)
            return;
        var tag = patchesQuery.ToComponentDataArray<RadiosityAddEnergyTag>(Allocator.TempJob);
        var positions = patchesQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
        var moveRay = patchesQuery.ToComponentDataArray<MoveRayRadiosity>(Allocator.TempJob);
        var rayProperties = patchesQuery.ToComponentDataArray<RayPropertiesRadiosity>(Allocator.TempJob);
        var entities = patchesQuery.ToEntityArray(Allocator.TempJob);
        
        var patchImpulseResponses = GetBufferFromEntity<PatchImpulseResponse>(true);
        var ecb = endSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.WithReadOnly(patchImpulseResponses).WithReadOnly(tag).WithReadOnly(entities).WithReadOnly(positions).WithReadOnly(moveRay)
            .WithReadOnly(rayProperties).ForEach((Entity entity, int entityInQueryIndex,
                ref DynamicBuffer<PatchImpulseResponseNew> impulseResponse,
                ref RadiosityPatch patch, in Translation translation
            ) =>
            {
                for (var i = 0; i < numEntities; i++)
                {
                    var cmp = patch.Normal == tag[i].Normal;
                    if (!(cmp.x && cmp.y))
                        continue;
                    var posDiff = math.abs(positions[i].Value - translation.Value);
                    // var cmp2 = posDiff <= (patch.Size / 2);
                    var found = false;
                    if (math.abs(tag[i].Normal.y) > 0)
                        found = posDiff.x <= (patch.Size.x / 2);
                    if (math.abs(tag[i].Normal.x) > 0)
                        found = posDiff.y <= (patch.Size.y / 2);

                    if (!found) continue;


                    patch.UnShotEnergy += rayProperties[i].Energy * (1f - patch.Absorption);

                    if (moveRay[i].Source == Entity.Null)
                    {
                        // directly coming from source
                        impulseResponse.Add(new PatchImpulseResponseNew()
                            { Distance = moveRay[i].Distance, Energy = rayProperties[i].Energy });
                    }
                    else
                    {
                        var sourceImpulseResponse = patchImpulseResponses[moveRay[i].Source];
                        // coming from patch
                        foreach (var sourceIr in sourceImpulseResponse)
                        {
                            var distance = moveRay[i].Distance + sourceIr.Distance;
                            var sourceIrEnergy = sourceIr.Energy * rayProperties[i].Energy;
                            if (sourceIrEnergy > 0)
                                impulseResponse.Add(new PatchImpulseResponseNew()
                                {
                                    Energy = sourceIrEnergy,
                                    Distance = distance
                                });
                        }
                    }
                    // lookup[entity].Add(new RadiosityReceivedEnergy()
                    // {
                    //     Distance = moveRay[i].Distance,
                    //     Energy = rayProperties[i].Energy,
                    //     Source = rayProperties[i].SourcePatch,
                    //     Index = rayProperties[i].Index
                    // });
                    // deleteTag[i] = new DeleteEntityTag() { Delete = true };
                    ecb.DestroyEntity(entities[i].Index, entities[i]);
                    break;
                }
                
            }).WithDisposeOnCompletion(tag).WithDisposeOnCompletion(positions).WithDisposeOnCompletion(rayProperties)
            .WithDisposeOnCompletion(moveRay).WithDisposeOnCompletion(entities).WithDisposeOnCompletion(patchImpulseResponses)
            .ScheduleParallel();
        
        endSimulationEcbSystem.AddJobHandleForProducer(Dependency);
    }
}