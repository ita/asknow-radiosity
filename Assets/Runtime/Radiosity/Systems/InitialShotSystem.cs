using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class InitialShotSystem : SystemBase
{
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;
    EntityQuery simulationTimeQuery;
    Random random;

    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();

        random = Random.CreateFromIndex(9129279);
    }


    protected override void OnUpdate()
    {
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        var prefab = Entity.Null;
        var position = float3.zero;
        Entities
            .ForEach((in RaySpawnerRadiosityComponent spawnerFromEntity,in Translation translation) =>
            { 
                prefab = spawnerFromEntity.Prefab;
                position = translation.Value;
            }).Run();
        
        if (prefab == Entity.Null) return;

        random.NextFloat();
        var random1 = random;
        Entities.WithAll<OnClickedTag>()
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref StartRandomRays startRandomRays) =>
            {
                if(startRandomRays.Shoot) return;

                var numParticles = startRandomRays.Number * 2;
                var deltaAngle = 2f * math.PI / numParticles;
                for (var i = 0; i < numParticles; i++)
                {
                    // var angle = random1.NextFloat(0, 360);
                    var angle = deltaAngle*i;
                    var instance = commandBuffer.Instantiate(entityInQueryIndex, prefab);
                    var moveDirection = new float3(math.cos(angle), math.sin(angle), 0);
                    commandBuffer.SetComponent(entityInQueryIndex, instance,
                        new Translation {Value = position});
                    commandBuffer.SetComponent(entityInQueryIndex, instance,
                        new Rotation() {Value = quaternion.RotateZ(angle+ (math.PI / 2f))});
                    commandBuffer.AddComponent<TowardsPatchTag>(entityInQueryIndex, instance);
                        commandBuffer.AddComponent(entityInQueryIndex, instance,
                        new MoveRayRadiosity() {Direction = math.normalize(moveDirection), Speed = 1f, Distance = 0f});
                    commandBuffer.AddComponent(entityInQueryIndex, instance,
                        new RayPropertiesRadiosity()
                        {
                            SourcePatch = entity,
                            Energy = 1f/numParticles,
                            // Energy = 1f,
                        });
                    startRandomRays.Shoot = true;
                }
            }).ScheduleParallel();
        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

}