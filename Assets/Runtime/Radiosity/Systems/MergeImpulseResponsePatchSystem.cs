﻿using Unity.Entities;
using Unity.Mathematics;

[UpdateAfter(typeof(AddRayToImpulseResponsePatchSystem))]
public class MergeImpulseResponsePatchSystem : SystemBase
{
    private const float DeltaDistance = 1f; 

    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, int entityInQueryIndex,
                ref DynamicBuffer<PatchImpulseResponseNew> impulseResponseNew,
                ref DynamicBuffer<PatchImpulseResponse> impulseResponse
            ) =>
            {
                foreach (var sourceIr in impulseResponseNew)
                {
                    var energyAdded = false;
                    var index = 0;
                    var distance =  sourceIr.Distance;
                    var sourceIrEnergy = sourceIr.Energy;
                    for (var j = 0; j < impulseResponse.Length; j++)
                    {
                        var ir = impulseResponse[j];
                        if (ir.Distance < distance)
                            index = j;
                        if (math.abs(ir.Distance - distance) < DeltaDistance)
                        {
                            impulseResponse[j] = new PatchImpulseResponse()
                            {
                                Energy = sourceIrEnergy + impulseResponse[j].Energy,
                                Distance = impulseResponse[j].Distance
                            };
                            energyAdded = true;
                            break;
                        }
                    }

                    if (!energyAdded)
                    {
                        impulseResponse.Insert(index, new PatchImpulseResponse()
                        {
                            Energy = sourceIrEnergy,
                            Distance = distance
                        });
                    }
                }
                impulseResponseNew.Clear();
            }).ScheduleParallel();
    }
}