using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using VBAecs.Histogram;

public class ResetSceneSystem : SystemBase
{
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;
    EntityQuery simulationTimeQuery;
    Random random;

    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();

        random = Random.CreateFromIndex(91259279);
    }

    // TODO implement properly
    protected override void OnUpdate()
    {
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        var resetScene = false;
        Entities.WithAll<ResetSceneTag,OnClickedTag>()
            .ForEach((Entity entity, int entityInQueryIndex) =>
            {
                resetScene = true;
            }).Run();
        
        if (!resetScene) return;

        // Reset Patches
        Entities
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref RadiosityPatch patch) =>
            {
                // reset Energy
                patch.Energy = 0f;
                patch.UnShotEnergy = 0f;
            }).ScheduleParallel();
        
        // Set 
        Entities
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((ref StartRandomRays start) =>
            {
                // reset Energy
                start.Shoot = false;
            }).ScheduleParallel();
        
        // Set 
        Entities
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((ref ReceiverTime receiverTime) =>
            {
                // reset Energy
                receiverTime.Start = false;
                receiverTime.CurrentTime = 0f;
            }).ScheduleParallel();
        
        // Destroy Rays
        Entities.WithAny<MoveRayRadiosity,MoveInHistogram>()
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex) =>
            {
                // reset Energy
                commandBuffer.DestroyEntity(entityInQueryIndex, entity);
            }).ScheduleParallel();
        
        // Clear Buffer
        Entities.ForEach((ref DynamicBuffer<PatchImpulseResponse> buffer) =>
            { 
                buffer.Clear();
            }).Schedule();

        // Clear Buffer
        Entities.ForEach((ref DynamicBuffer<PatchImpulseResponseNew> buffer) =>
            { 
                buffer.Clear();
            }).Schedule();


        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

}