﻿using Unity.Entities;
using UnityEngine;

public class ActivateButtonSystem : SystemBase
{
    private BeginInitializationEntityCommandBufferSystem entityCommandBufferSystem;

    protected override void OnCreate()
    {
        entityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var commandBuffer = entityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        var firstShotDone = false;
        Entities.WithBurst()
            .ForEach((ref StartRandomRays start) =>
            {
                firstShotDone = start.Shoot;
            }).Run();

        var isFinilizing = false;
        Entities.WithBurst()
            .ForEach((ref ReceiverTime spawnerFromEntity) =>
            {
                isFinilizing= spawnerFromEntity.Start;
            }).Run();

        Entities.WithoutBurst().WithChangeFilter<StartRandomRays>().ForEach(
            (Entity entity, int entityInQueryIndex, SpriteRenderer renderer, in StartRandomRays start) =>
            {
                renderer.enabled = !isFinilizing;
            }).Run();
        
        Entities.WithoutBurst().ForEach(
            (Entity entity, int entityInQueryIndex, SpriteRenderer renderer, in ReceiverTime receiver) =>
            {
                renderer.enabled = firstShotDone && !isFinilizing;
            }).Run();
        
        
        Entities.WithoutBurst().ForEach(
            (Entity entity, int entityInQueryIndex, SpriteRenderer renderer, in RenderPatch patch) =>
            {
                renderer.enabled = !isFinilizing && firstShotDone;
            }).Run();

        var renderShotEnergy = false;
        Entities.WithoutBurst().ForEach(
            (Entity entity, int entityInQueryIndex, in RenderPatch patch) =>
            {
                renderShotEnergy = patch.RenderShotEnergy;
            }).Run();
        
        Entities.WithoutBurst().WithAll<EnergyStatusTag>().ForEach(
            (Entity entity, int entityInQueryIndex, TextMesh text) =>
            {
                text.text = renderShotEnergy ? "show unshot energy" : "show shot energy";
            }).Run();
        
        Entities.WithoutBurst().WithAll<StartButtonTag>().ForEach(
            (Entity entity, int entityInQueryIndex, TextMesh text) =>
            {
                text.text = firstShotDone ? "Next" : "Start";
                text.text = isFinilizing ? "" : text.text;
            }).Run();
        
        Entities.WithoutBurst().WithAll<FinalizeButtonTag>().ForEach(
            (Entity entity, int entityInQueryIndex, TextMesh text) =>
            {
                text.text = firstShotDone && !isFinilizing ? "Finalize" : "";
            }).Run();
        
        
        Entities.WithoutBurst().WithAll<ChangeEnergyButtonTag>().ForEach(
            (Entity entity, int entityInQueryIndex, TextMesh text) =>
            {
                text.text = !isFinilizing && firstShotDone ? "Change Energy" : "";
            }).Run();
        
        entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

}