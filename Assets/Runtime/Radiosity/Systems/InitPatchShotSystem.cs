using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

[UpdateBefore(typeof(PatchShotSystem))]
[UpdateBefore(typeof(InitialShotSystem))]
public class InitPatchShotSystem : SystemBase
{
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }


    protected override void OnUpdate()
    {
        var patchesQuery = GetEntityQuery(
            ComponentType.ReadOnly<RadiosityPatch>());
        var numEntities = patchesQuery.CalculateEntityCount();
        
        var radiosityPatches = patchesQuery.ToComponentDataArray<RadiosityPatch>(Allocator.TempJob);
        var radiosityPatchesEntities = patchesQuery.ToEntityArray(Allocator.TempJob);
        
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        var indexMaxPatch = -1;
        // var maxIrTime = 0f;
        // var deltaTimeIr = 0f;
        Entities.WithAll<OnClickedTag>()
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref StartRandomRays startRandomRays) =>
            {
                // maxIrTime = startRandomRays.MaxImpulseResponseTime;
                // deltaTimeIr = startRandomRays.ImpulseResponseDeltaTime;
                if (!startRandomRays.Shoot) return;

                var maxEnergy = 0f;
                var maxIndex = -1;
                for (var i = 0; i < numEntities; i++)
                {
                    if (!(radiosityPatches[i].UnShotEnergy > maxEnergy)) continue;

                    maxEnergy = radiosityPatches[i].UnShotEnergy;
                    maxIndex = i;
                }

                indexMaxPatch = radiosityPatchesEntities[maxIndex].Index;
            }).WithDisposeOnCompletion(radiosityPatches).WithDisposeOnCompletion(radiosityPatchesEntities)
            .Run();
        
        Entities.WithAll<RadiosityPatch>()
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex) =>
            {
                if (indexMaxPatch == entity.Index)
                    commandBuffer.AddComponent(entityInQueryIndex, entity, new HighestUnshotEnergyTag());
                
                // var irbuffer = commandBuffer.AddBuffer<PatchImpulseResponse>(entityInQueryIndex, entity);
                // for (var time = 0f; time < maxIrTime; time +=deltaTimeIr)
                // {
                //     irbuffer.Add(new PatchImpulseResponse() { TimeStamp = time, Energy = 0f });
                // }
            }).ScheduleParallel();
        
        
        
        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}