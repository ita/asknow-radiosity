using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using VBAecs.Histogram;

public class MoveRayRadiositySystem : SystemBase
{
    private const float XLimitLow = -9.13f+0.15f;
    private const float XLimitUp1 = 0.8699997f-0.15f;
    private const float XLimitUp2 = -2.13f-0.15f;
    private const float YLimitLow = -3.95f+0.15f;
    private const float YLimitUp = 4.05f-0.15f;
    private const float MiddleLimit = 0.05f-0.15f;
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
    protected override void OnCreate()
    {
        base.OnCreate();
        // Find the ECB system once and store it for later usage
        m_EndSimulationEcbSystem = World
            .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var speed = 0f;
        Entities.ForEach((in SpeedData speedData) =>
        {
            speed = speedData.Value;
        }).Run();
        var timeDeltaTime = Time.DeltaTime;
       var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();

       Entities.WithAll<TowardsPatchTag>().WithNone<RadiosityAddEnergyTag>().ForEach(
           (Entity entity, int entityInQueryIndex, ref MoveRayRadiosity moveTo, ref Translation translation) =>
           {
               if (moveTo.Speed == 0)
                   return;
               var deltaWay = moveTo.Direction * moveTo.Speed * timeDeltaTime * speed;
               var moveToDirection = translation.Value + deltaWay;
               var normal = float3.zero;
               // Left Wall
               if (moveToDirection.x < XLimitLow)
               {
                   moveToDirection.x = XLimitLow;

                   normal = new float3(1, 0, 0);

               }
               // Wall down
               else if (moveToDirection.y < YLimitLow)
               {
                   moveToDirection.y = YLimitLow;
                   normal = new float3(0, 1, 0);
               }
               // Wall up
               else if (moveToDirection.y > YLimitUp)
               {
                   moveToDirection.y = YLimitUp;
                   normal = new float3(0, -1, 0);
               }
               // horizontal Plane
               else if (moveToDirection.y >= MiddleLimit && translation.Value.x < XLimitUp1 &&
                        translation.Value.x > XLimitUp2
                        && moveToDirection.x < XLimitUp1 && moveToDirection.x > XLimitUp2 &&
                        moveToDirection.y - MiddleLimit < 1)
               {
                   if (moveToDirection.y - MiddleLimit > 1f)
                   {
                       var a = 0;
                       a++;
                   }

                   moveToDirection.y = MiddleLimit;
                   normal = new float3(0, -1, 0);
               }
               // Wall unten rechts
               else if (moveToDirection.x > XLimitUp1 && translation.Value.y < MiddleLimit &&
                        moveToDirection.y < MiddleLimit)
               {
                   if (moveToDirection.x - XLimitUp1 > 1f)
                   {
                       var a = 0;
                       a++;
                   }

                   moveToDirection.x = XLimitUp1;
                   normal = new float3(-1, 0, 0);
               }
               // Wall oben rechts
               else if (moveToDirection.x > XLimitUp2 && translation.Value.y >= MiddleLimit &&
                        moveToDirection.y >= MiddleLimit)
               {
                   if (moveToDirection.x - XLimitUp2 > 1f)
                   {
                       var a = 0;
                       a++;
                   }

                   moveToDirection.x = XLimitUp2;
                   normal = new float3(-1, 0, 0);
               }
               else
               {
                   moveTo.Distance += math.length(deltaWay);
                   translation.Value = moveToDirection;
                   return;
               }

               ecb.AddComponent(entityInQueryIndex, entity, new RadiosityAddEnergyTag() { Normal = normal });
           }).ScheduleParallel();

       var receiverPosition = float3.zero;
       Entities.WithAll<ReceiverTag>().ForEach((in Translation translation) =>
       {
           receiverPosition = translation.Value;
       }).Run();
       
       var deltaWay1 = 0f;
       var zeroPos = float3.zero;
       Entities.ForEach((in HistogramData histogram) =>
       {
           deltaWay1 = histogram.DeltaWay;
           zeroPos = histogram.Zero;
       }).Run();
       
       Entities.WithAll<TowardsReceiverTag>().WithNone<RadiosityAddEnergyTag>().ForEach(
           (Entity entity, int entityInQueryIndex, ref MoveRayRadiosity moveTo, ref Translation translation, in RayPropertiesRadiosityReceiver prop) =>
           {
               if (moveTo.Speed == 0)
                   return;
               var deltaWay = moveTo.Direction * moveTo.Speed * timeDeltaTime * speed*2f;
               var moveToDirection = translation.Value + deltaWay;
               var distanceBefore = math.distance(translation.Value, receiverPosition);
               var distanceAfter = math.distance(moveToDirection, receiverPosition);
               moveTo.Distance += math.length(deltaWay);

               var increaseDistanceToReceiver = distanceBefore < distanceAfter;
               
               translation.Value = moveToDirection;
               
               if (distanceAfter < 0.2f || increaseDistanceToReceiver)
               {
                   // Ray received
                   moveTo.Speed = 0f;
                   var goal = zeroPos;
                   var bla = moveTo.Distance/4f;
                   goal.x += bla * deltaWay1;
                   var energy = prop.Energy / moveTo.Distance;
                   energy = 1f * math.log10(energy/1e-40f)*31.25e-4f;
                   if(energy<=0f)
                   {
                       ecb.DestroyEntity(entityInQueryIndex, entity);
                       return;
                   }
                   var newTrans = receiverPosition;
                   newTrans.x = goal.x;
                   newTrans.y = 4.5f;
                   ecb.SetComponent(entityInQueryIndex, entity,
                       new Translation()
                           {Value = newTrans});
                   ecb.SetComponent(entityInQueryIndex, entity,
                       new Rotation()
                           {Value = quaternion.RotateZ(0f)});
                   ecb.AddComponent(entityInQueryIndex, entity,
                       new MoveInHistogram() {Energy = energy, Position = goal, Speed = 2f});
                   ecb.RemoveComponent<MoveRayRadiosity>(entityInQueryIndex, entity);
               }
           }).ScheduleParallel();
       
        
        m_EndSimulationEcbSystem.AddJobHandleForProducer(Dependency);

    }
}