﻿using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class ReceiverShotSystem : SystemBase
{
    private BeginInitializationEntityCommandBufferSystem entityCommandBufferSystem;

    protected override void OnCreate()
    {
        entityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var commandBuffer = entityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        
        Entities.WithAll<OnClickedTag>().WithBurst()
            .ForEach((ref ReceiverTime spawnerFromEntity) =>
            {
                spawnerFromEntity.Start = true;
            }).Run();

        
        var currentTimeStep = 0f;
        var timeDeltaTime = Time.DeltaTime;
        var running = false;
        Entities.WithBurst()
            .ForEach((ref ReceiverTime spawnerFromEntity) =>
            {
                running = spawnerFromEntity.Start;
                if (running)
                {
                    currentTimeStep = spawnerFromEntity.CurrentTime;
                    spawnerFromEntity.CurrentTime += timeDeltaTime;
                }

            }).Run();

        if (!running)
            return;
        

        var prefab = Entity.Null;
        Entities
            .ForEach((in RaySpawnerRadiosityComponent spawnerFromEntity) =>
            {
                prefab = spawnerFromEntity.Prefab;
            }).Run();
        
        var receiverPosition = float3.zero;
        Entities.WithAll<ReceiverTag>()
            .ForEach((in Translation translation) =>
            {
                receiverPosition = translation.Value;

            }).Run();
        
        Entities
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref DynamicBuffer<PatchImpulseResponse> shootToReceivers,
                in Translation translation) =>
            {
                var shotEnergy = 0f;
                var distance = 0f;
                for (var i = shootToReceivers.Length - 1; i >= 0; i--)
                {
                    var shootToReceiver = shootToReceivers[i];
                    if (!(shootToReceiver.Distance/3.43f/2f*6f/5f < currentTimeStep)) continue;

                    // Collect Energy
                    shotEnergy += shootToReceiver.Energy;
                    distance += shootToReceiver.Distance;

                    // Remove Element
                    shootToReceivers.RemoveAt(i);
                }

                if (shotEnergy == 0f) return;
                
                // Create 
                var pos = translation.Value;
                var moveDirection = receiverPosition - pos;
                var angle = math.atan2(moveDirection.y, moveDirection.x);
                var instance = commandBuffer.Instantiate(entityInQueryIndex, prefab);

                
                var checkEnergy = 1f * math.log10(shotEnergy/distance/1e-40f)*31.25e-4f;
                if (checkEnergy < 0)
                    return;
                commandBuffer.SetComponent(entityInQueryIndex, instance,
                    new Translation {Value = pos});
                commandBuffer.SetComponent(entityInQueryIndex, instance,
                    new Rotation() {Value = quaternion.RotateZ(angle + (math.PI / 2f))});
                commandBuffer.AddComponent(entityInQueryIndex, instance,
                    new MoveRayRadiosity()
                    {
                        Direction = math.normalize(moveDirection), 
                        Speed = 1f,
                        Distance = distance
                    });

                commandBuffer.AddComponent<TowardsReceiverTag>(entityInQueryIndex, instance);
                commandBuffer.AddComponent(entityInQueryIndex, instance,
                    new RayPropertiesRadiosityReceiver()
                    {
                        Time = currentTimeStep,
                        Energy = shotEnergy,
                    });
                
            }).ScheduleParallel();
        
        Entities
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref RadiosityPatch patch, ref Scale scale) =>
            {
                patch.UnShotEnergy = 0;
                patch.Energy = 0;
                scale.Value = 0;
            }).ScheduleParallel();
        entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

}