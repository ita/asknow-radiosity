using Unity.Entities;

[GenerateAuthoringComponent]
public struct RenderPatch : IComponentData
{
    public bool RenderShotEnergy;
    public float Factor;
    public bool UseLogScale;
}