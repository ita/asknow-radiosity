using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class RadiosityWall : MonoBehaviour
{
    [SerializeField] private int numberPatches;
    [SerializeField] private float length;
    [SerializeField] private float3 normal;
    [SerializeField] private GameObject prefab;
    [SerializeField] private Material material;
    [SerializeField] private Entity AsteroidEntityPrefab;
    [SerializeField] private Vector3 angle;
    [SerializeField][Range(0,1)] private float absorption=0.3f;
    

    [ContextMenu("Create Mesh")]
    private void CreateCircle()
    { //mesh = UpdateMesh();
    }
    
    private void Awake()
    {
        var dstManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var lengthPatch = length / numberPatches;
        var posWall = transform.position;
        var dirWall = math.abs(normal.x) > 0.5f ? Vector3.up : Vector3.right;
        var posStart = posWall - (dirWall * length / 2);
        Mesh spriteMesh = CreateMesh(1f, 1f);
        prefab.transform.rotation = Quaternion.Euler(angle);
        AsteroidEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab,  new  GameObjectConversionSettings(World.DefaultGameObjectInjectionWorld, GameObjectConversionUtility.ConversionFlags.AssignName)  );

        for (var i = 0; i < numberPatches; i++)
        {
            var entity1 = dstManager.Instantiate(AsteroidEntityPrefab);

            float3 pos = posStart + dirWall * ((lengthPatch * i) + (lengthPatch / 2));
            float3 size = lengthPatch * dirWall;
            dstManager.SetComponentData(entity1,new Translation(){Value = pos});
            // dstManager.RemoveComponent<NonUniformScale>(entity1);
            dstManager.AddComponentData(entity1, new Scale(){Value = 0f});
            // dstManager.AddComponentData(entity1, new RenderBounds());
            dstManager.AddBuffer<PatchImpulseResponse>(entity1);
            dstManager.AddBuffer<PatchImpulseResponseNew>(entity1);
            // dstManager.AddComponentData(entity1,
            //     new Rotation() {Value = quaternion.LookRotation(normal, new float3(1, 0, 0))});
            dstManager.AddComponentData(entity1,
            new RadiosityPatch() {Normal = normal, Energy = 0f, UnShotEnergy = 0f, Size = size, Absorption = absorption});
        }
        dstManager.DestroyEntity(AsteroidEntityPrefab);
        
    }
    private void Awake1()
    {
        var dstManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var entityArchetype = dstManager.CreateArchetype(
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(Translation),
            typeof(Rotation),
            typeof(Scale),
            typeof(RenderBounds),
            typeof(RadiosityPatch)
        );
        var lengthPatch = length / numberPatches;
        var posWall = transform.position;
        var dirWall = math.abs(normal.x) > 0.5f ? Vector3.up : Vector3.right;
        var posStart = posWall - (dirWall * length / 2);
        Mesh spriteMesh = CreateMesh(1f, 1f);


        for (var i = 0; i < numberPatches; i++)
        {
            float3 pos = posStart + dirWall * ((lengthPatch * i) + (lengthPatch / 2));
            float3 size = lengthPatch * dirWall;
            var entity1 = dstManager.CreateEntity(entityArchetype);
            dstManager.SetSharedComponentData(entity1, new RenderMesh() {
                material = material,
                mesh = spriteMesh,
                
            });
            dstManager.SetComponentData(entity1,new Translation(){Value = pos});
            // dstManager.SetComponentData(entity1,
            //   new Rotation() {Value = quaternion.LookRotation(normal, new float3(0, 0, 1))});
            dstManager.SetComponentData(entity1,
                new Scale() {Value = 10f});
            dstManager.AddBuffer<PatchImpulseResponse>(entity1);
            dstManager.SetComponentData(entity1,
                new Rotation() {Value = quaternion.LookRotation(normal, new float3(0, 0, 1))});
            dstManager.SetComponentData(entity1,
                new RadiosityPatch() {Normal = normal, Energy = 0f, UnShotEnergy = 0f, Size = size});
        }
        
    }
    private Mesh CreateMesh(float width, float height) {
        Vector3[] vertices = new Vector3[4];
        Vector2[] uv = new Vector2[4];
        int[] triangles = new int[6];

        /* 0, 0
         * 0, 1
         * 1, 1
         * 1, 0
         * */

        float halfWidth = width / 2f;
        float halfHeight = height / 2f;

        vertices[0] = new Vector3(-halfWidth, -halfHeight);
        vertices[1] = new Vector3(-halfWidth, +halfHeight);
        vertices[2] = new Vector3(+halfWidth, +halfHeight);
        vertices[3] = new Vector3(+halfWidth, -halfHeight);

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(0, 1);
        uv[2] = new Vector2(1, 1);
        uv[3] = new Vector2(1, 0);

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 3;

        triangles[3] = 1;
        triangles[4] = 2;
        triangles[5] = 3;

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;

        return mesh;
    }



}