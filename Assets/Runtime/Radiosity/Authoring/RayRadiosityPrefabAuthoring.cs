using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class RayRadiosityPrefabAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField] private Material material;
    
    

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        var prefabArchetype = dstManager.CreateArchetype(
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(Translation),
            typeof(Rotation),
            typeof(Scale),
            typeof(RenderBounds)
        );
        var prefab = dstManager.CreateEntity(prefabArchetype);
        dstManager.SetSharedComponentData(prefab, new RenderMesh {
            mesh = MeshCreator.TriangleMeshCenter(0.1f,0.5f),
            material = material,
        });
        dstManager.SetComponentData(prefab,
            new Scale() {Value = 1f});
        dstManager.SetComponentData(prefab,
            new Translation() {Value = new float3(0,-100,-100)});

        dstManager.AddComponentData(entity, new RaySpawnerRadiosityComponent() {Prefab = prefab});

    }
}