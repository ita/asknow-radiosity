using Unity.Entities;

[GenerateAuthoringComponent]
public struct NumRaysElement : IComponentData
{
    public int NumberRays;
}