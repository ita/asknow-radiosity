using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace VBAecs.Histogram
{    
    [UpdateInGroup(typeof(HistogramSystemGroup))]
    public class HistogramSpawnerSystem : SystemBase
    {
        BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

        protected override void OnCreate()
        {
            m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        }


        protected override void OnUpdate()
        {
            var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

            Entities
                .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
                .ForEach((Entity entity, int entityInQueryIndex, ref HistogramData spawner) =>
                {
                    if (spawner.Created)
                        return;
                    for (int i = 0; i < spawner.Number; i++)
                    {
                        var instance = commandBuffer.Instantiate(entityInQueryIndex, spawner.Prefab);
                        var position = new float3(spawner.Zero);
                        position.x += i * spawner.DeltaWay;
                        var scale = new float3(spawner.DeltaWay, 0, 1);
                        commandBuffer.SetComponent(entityInQueryIndex, instance, new Translation {Value = position});
                        commandBuffer.SetComponent(entityInQueryIndex, instance, new NonUniformScale() {Value = scale});

                    }

                    spawner.Created = true;

                }).ScheduleParallel();
            m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        }

    }
}