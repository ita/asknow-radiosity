using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace VBAecs.Histogram
{
    [UpdateInGroup(typeof(HistogramSystemGroup))]
    public class AddEnergyToHistogramSystem : SystemBase
    {
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            // Find the ECB system once and store it for later usage
            m_EndSimulationEcbSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var deltaWay = 0f;
            Entities.ForEach((in HistogramData histogram) => { deltaWay = histogram.DeltaWay; }).Run();

            var histogramQuery = GetEntityQuery(
                ComponentType.ReadOnly<EnergyToHistogramData>(),
                ComponentType.ReadOnly<Translation>());
            var numEntities = histogramQuery.CalculateEntityCount();
            var energy = histogramQuery.ToComponentDataArray<EnergyToHistogramData>(Allocator.TempJob);
            var positions = histogramQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
            var entities = histogramQuery.ToEntityArray(Allocator.TempJob);

            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
            Entities.ForEach((ref NonUniformScale scale, in Translation translation, in HistogramTag tag) =>
                {
                    for (int i = 0; i < numEntities; i++)
                    {
                        if (math.abs(positions[i].Value.x - translation.Value.x) < deltaWay &&
                            positions[i].Value.x > translation.Value.x)
                        {
                            scale.Value.y += energy[i].Energy / 2;
                            if (scale.Value.y > 8f)
                            {
                                // Debug.Log(scale.Value.y + " > 8");
                                scale.Value.y = 8f;
                            }
                            // if (scale.Value.y < 1e-5f)
                            //     Debug.Log(scale.Value.y + " < 1e-5f");
                            ecb.DestroyEntity(entities[i].Index, entities[i]);
                        }
                    }
                }).WithDisposeOnCompletion(energy).WithDisposeOnCompletion(positions).WithDisposeOnCompletion(entities)
                .Schedule();
            m_EndSimulationEcbSystem.AddJobHandleForProducer(Dependency);
        }
    }
}