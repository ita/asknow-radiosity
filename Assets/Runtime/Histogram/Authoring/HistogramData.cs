using Unity.Entities;
using Unity.Mathematics;

namespace VBAecs.Histogram
{
    [GenerateAuthoringComponent]
    public struct HistogramData : IComponentData
    {
        public float3 Zero;
        public int Number;
        public float DeltaWay;
        public bool Created;
        public Entity Prefab;
    }
}