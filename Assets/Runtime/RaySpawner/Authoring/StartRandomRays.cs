using Unity.Entities;

[GenerateAuthoringComponent]
public struct StartRandomRays : IComponentData
{
    public int Number;
    public bool Shoot;
    // public float MaxImpulseResponseTime;
    // public float ImpulseResponseDeltaTime;
}