using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct RemoveRaysTag : IComponentData{}